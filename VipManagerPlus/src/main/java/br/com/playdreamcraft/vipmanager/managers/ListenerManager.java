package br.com.playdreamcraft.vipmanager.managers;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import br.com.playdreamcraft.vipmanager.events.JoinEvent;

public class ListenerManager {
	PluginManager plm;
	Plugin plugin;
	public ListenerManager(Plugin plugin) {
		plm = plugin.getServer().getPluginManager();
		this.plugin = plugin;
	}
	public void registerEvents(){
		plm.registerEvents(new JoinEvent(plugin), plugin);
	}
}
