package br.com.playdreamcraft.vipmanager.profiles;

import java.util.Date;

public class VipProfile {
	String user;
	String servidor;
	String plano;
	int duracao;
	String reference;
	String dataFinal;
	String dataInicial;
	int tempoRestante;
	boolean isNull;
	Date dataFinalDate;
	Date dataInitialDate;
	public boolean isNull() {
		return isNull;
	}
	public void setNull(boolean isNull) {
		this.isNull = isNull;
	}
	public VipProfile(String user, String servidor, String plano, int duracao, String reference, String dataFinal,
			String dataInicial) {
		this.user = user;
		this.servidor = servidor;
		this.plano = plano;
		this.duracao = duracao;
		this.reference = reference;
		this.dataFinal = dataFinal;
		this.dataInicial = dataInicial;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getServidor() {
		return servidor;
	}
	public void setServidor(String servidor) {
		this.servidor = servidor;
	}
	public String getPlano() {
		return plano;
	}
	public void setPlano(String plano) {
		this.plano = plano;
	}
	public int getDuracao() {
		return duracao;
	}
	
	public void setDuracao(int duracao) {
		this.duracao = duracao*30;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	public String getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	public int getTempoRestante() {
		return tempoRestante;
	}
	public void setTempoRestante(int tempoRestante) {
		this.tempoRestante = tempoRestante;
	}
	
}
