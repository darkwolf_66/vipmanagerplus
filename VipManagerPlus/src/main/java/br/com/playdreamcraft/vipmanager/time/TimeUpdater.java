package br.com.playdreamcraft.vipmanager.time;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;
public class TimeUpdater {
	String TIME_SERVER;
	NTPUDPClient timeClient;
	InetAddress inetAddress;
	TimeInfo timeInfo;
	Date time;
	public TimeUpdater() {
		TIME_SERVER = "a.ntp.br";
		timeClient = new NTPUDPClient();
	}
	public Date getTime(){
		try {
			inetAddress = InetAddress.getByName(TIME_SERVER);
			timeInfo = timeClient.getTime(inetAddress);
			long returnTime = timeInfo.getReturnTime();
			time = new Date(returnTime);
			return time;
		} catch (UnknownHostException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
}
