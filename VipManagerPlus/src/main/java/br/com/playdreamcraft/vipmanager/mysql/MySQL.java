package br.com.playdreamcraft.vipmanager.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class MySQL{
	private final String user;
	private final String database;
	private final String password;
	private final String port;
	private final String hostname;


	public MySQL(DatabaseProfile profile) {
		this.hostname = profile.getIp();
		this.port = profile.getPort();
		this.database = profile.getDatabase();
		this.user = profile.getUser();
		this.password = profile.getPassword();
	}

	public void refreshConnection() {
		if (connection == null) {
			try {
				openConnection();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public Connection openConnection() throws SQLException,
	ClassNotFoundException {
	//	if (checkConnection()) {
	//		return connection;
	//	}
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://"
				+ this.hostname + ":" + this.port + "/" + this.database,
				this.user, this.password);
		return connection;
	}
	
	public ResultSet querySQL(String query) throws SQLException,
	ClassNotFoundException {
		if (!checkConnection()) {
			openConnection();
		}

		Statement statement = connection.createStatement();		
		
		ResultSet result = statement.executeQuery(query);
		
	
		
		return result;
	}
	
	
	
	public int updateSQL(String query) throws SQLException,
	ClassNotFoundException {
		if (!checkConnection()) {
			openConnection();
		}

		Statement statement = connection.createStatement();

		int result = statement.executeUpdate(query);
		
		return result;
	}
	public void closeConnection(){
		try
		{
			if(connection != null)
				connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}
	protected Connection connection;

	public boolean checkConnection() throws SQLException {
		return connection != null && !connection.isClosed();
	}
}
