package br.com.playdreamcraft.vipmanager.commands.runnables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanager.managers.DatabaseVipManager;
import br.com.playdreamcraft.vipmanager.profiles.VipProfile;
import br.com.playdreamcraft.vipmanager.time.DateCalculator;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class TempoVipThread implements Runnable {
	Player player;
	String nick;
	DateCalculator dateCalc;
	DatabaseVipManager dbVipManager;
	VipProfile vipProfile;
	SimpleDateFormat sdf;
	SimpleDateFormat sdfParse;
	Date inicialDD;
	Date finalDD;
	String plano;
	String tempoRestante;
	double porcentagem;
	public TempoVipThread(Player player) {
		this.player = player;
	}

	@Override
	public void run() {
		nick = player.getName();
		dateCalc = new DateCalculator();
		dbVipManager = new DatabaseVipManager();
		vipProfile = dbVipManager.vipProfileCheck(nick, player, false);

		if (vipProfile.isNull()) {
			player.sendMessage(
					VipManagerPlus.tagManager.erro() + "N�o possui vip. Adquira vip em loja.playdreamcraft.com.br");
		} else if (dateCalc.verifyDate(dateCalc.stringToDate(vipProfile.getDataFinal()))) {
			player.sendMessage(
					VipManagerPlus.tagManager.erro() + "VIP expirado. Adquira vip em loja.playdreamcraft.com.br");
		} else {
			sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
			sdfParse = new SimpleDateFormat("dd/MM/yyyy-HH-mm");
			try {
				finalDD = new Date(sdfParse.parse(vipProfile.getDataFinal()).getTime());
				inicialDD = new Date(sdfParse.parse(vipProfile.getDataInicial()).getTime());

				player.sendMessage(
						ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "#############################################");
				player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "################# " + ChatColor.AQUA + ""
						+ ChatColor.BOLD + "VIP ATIVO!!!" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD
						+ " #################");
				player.sendMessage(
						ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "#############################################");
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Nick: " + ChatColor.GRAY + "" + ChatColor.BOLD
						+ "" + player.getName());
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Servidor: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + vipProfile.getServidor());

				
				if (vipProfile.getPlano().equalsIgnoreCase("ferro")) {
					plano = ChatColor.GRAY + "" + ChatColor.BOLD + vipProfile.getPlano();
				} else if (vipProfile.getPlano().equalsIgnoreCase("ouro")) {
					plano = ChatColor.GOLD + "" + ChatColor.BOLD + vipProfile.getPlano();
				} else if (vipProfile.getPlano().equalsIgnoreCase("diamante")) {
					plano = ChatColor.BLUE + "" + ChatColor.BOLD + vipProfile.getPlano();
				} else if (vipProfile.getPlano().equalsIgnoreCase("esmeralda")) {
					plano = ChatColor.GREEN + "" + ChatColor.BOLD + vipProfile.getPlano();
				} else if (vipProfile.getPlano().equalsIgnoreCase("obsidian")) {
					plano = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + vipProfile.getPlano();
				} else {
					plano = ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + vipProfile.getPlano();
				}

				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Plano: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + plano);
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Data Inicial: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + sdf.format(inicialDD).toString());
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Data Final: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + sdf.format(finalDD).toString());
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Dura��o: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + vipProfile.getDuracao() * 30 + " Dias");

				porcentagem = (100 * vipProfile.getTempoRestante()) / vipProfile.getDuracao();
				if (porcentagem < 5) {
					tempoRestante = ChatColor.RED + "" + ChatColor.BOLD + "" + vipProfile.getTempoRestante() + " Dias";
				} else if (porcentagem > 5 && porcentagem < 20) {
					tempoRestante = ChatColor.GOLD + "" + ChatColor.BOLD + "" + vipProfile.getTempoRestante() + " Dias";
				} else if (porcentagem > 20 && porcentagem < 60) {
					tempoRestante = ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "" + vipProfile.getTempoRestante()
					+ " Dias";
				} else {
					tempoRestante = ChatColor.GREEN + "" + ChatColor.BOLD + "" + vipProfile.getTempoRestante() + " Dias";
				}
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "  Tempo Restante: " + tempoRestante);
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "" + "  Codigo: " + ChatColor.GRAY + ""
						+ ChatColor.BOLD + "" + vipProfile.getReference());
				player.sendMessage(
						ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "#############################################");

			} catch (ParseException e) {
				player.sendMessage(VipManagerPlus.tagManager.erro()
						+ "Erro Critico!!! Avise o administrador contato@playdreamcraft.com.br");
			}
		}
	}
}
