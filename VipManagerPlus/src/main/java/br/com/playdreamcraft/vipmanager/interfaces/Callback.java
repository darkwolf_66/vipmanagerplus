package br.com.playdreamcraft.vipmanager.interfaces;

public interface Callback<T> {
	void onSuccess(T done);

	void onFailure(Throwable cause);
}
