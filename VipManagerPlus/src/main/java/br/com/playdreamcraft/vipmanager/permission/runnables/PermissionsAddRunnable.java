package br.com.playdreamcraft.vipmanager.permission.runnables;

import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanagerplus.apimanagers.VaultApi;

public class PermissionsAddRunnable implements Runnable{
	Player player;
	String plano;
	public PermissionsAddRunnable(Player player, String plano) {
		this.plano = plano;
		this.player = player;
	}
	@Override
	public void run() {
		if(VaultApi.perms.playerInGroup(player, plano)){
			VaultApi.perms.playerAddGroup(player, plano);
		}
	}

}
