package br.com.playdreamcraft.vipmanager.commands;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.managers.DatabaseVipManager;
import br.com.playdreamcraft.vipmanager.profiles.VipProfile;
import br.com.playdreamcraft.vipmanager.time.DateCalculator;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.apimanagers.VaultApi;

public class CommandAddVip implements CommandExecutor {
	Plugin p;
	public CommandAddVip(Plugin p){
		this.p = p;
	}
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(!(arg0 instanceof Player)) {
			arg0.sendMessage(VipManagerPlus.tagManager.erro()+"Somente players podem usar este comando!");
			return true;
		}
		DatabaseVipManager dbVipManager = new DatabaseVipManager();
		Player player = (Player)arg0;
		if(!player.isOp()){
			arg0.sendMessage(VipManagerPlus.tagManager.erro()+"Somente diretores pode executar este comando!");
			return true;
		}
		
		try{
			arg0.sendMessage(VipManagerPlus.tagManager.normal()+"Nick: "+arg3[0]);
			arg0.sendMessage(VipManagerPlus.tagManager.normal()+"Plano: "+arg3[1]);
			arg0.sendMessage(VipManagerPlus.tagManager.normal()+"Tempo: "+arg3[2]);
		}catch (Exception e) {
			arg0.sendMessage(VipManagerPlus.tagManager.erro()+"Algum parametro em branco! Para acionar vip /addvip nick plano dias");
			return true;
		}
		if(dbVipManager.checkNick(arg3[0])){
			arg0.sendMessage(VipManagerPlus.tagManager.erro()+"J� possui vip!");
			return true;
		}
		DateCalculator dateCalc = new DateCalculator();

		String nick = arg3[0];
		String plano = arg3[1];
		int duracao = Integer.parseInt(arg3[2]);
		Calendar c = Calendar.getInstance();
		c.setTime(dateCalc.dataAtual());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + duracao);
		String dataFinal = new SimpleDateFormat("dd/MM/yyyy-HH-mm").format(c.getTime());
		String dataInicial = new SimpleDateFormat("dd/MM/yyyy-HH-mm").format(dateCalc.dataAtual());
		
		String servidor = VipManagerPlus.configManager.getServidorcg();
		
		StringBuilder reference = new StringBuilder();
		SimpleDateFormat spDate = new SimpleDateFormat("yyyyMMddHHmm");
		reference.append(spDate.format(dateCalc.dataAtual()));
		reference.append("-");
		reference.append(plano);
		reference.append("-");
		if(duracao>=30){
			duracao = duracao/30;
		}else{
			duracao = 1;
		}
		
		reference.append(duracao);
		reference.append("-");
		reference.append(servidor);
		reference.append("-");
		reference.append(nick);
		
		
		VipProfile newVip = new VipProfile(nick, servidor, plano, duracao, reference.toString(), dataFinal, dataInicial);
		
		dbVipManager.addVipApi(newVip, player);
		player.sendMessage(VipManagerPlus.tagManager.normal()+"Vip adicionado!!");
		VipManagerPlus.logger.logNormal("Vip: "+plano+" adicionado para: "+nick+" player que adicionou o vip:"+player.getName());
		VaultApi.getInstance();
		VaultApi.perms.playerAddGroup(player, plano);
		return true;
	}

}
