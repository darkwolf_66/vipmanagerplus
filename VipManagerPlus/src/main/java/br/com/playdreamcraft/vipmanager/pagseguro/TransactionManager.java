package br.com.playdreamcraft.vipmanager.pagseguro;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.bukkit.entity.Player;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class TransactionManager {
	public Transaction consultar(String arg, Player p){
		String urlcxz = "https://ws.pagseguro.uol.com.br/v2/transactions?email=jurandird@bol.com.br&token=1A5EA14735C54FC8800BE18390B4205F&reference="+arg;
		URL urlcx;
		Transaction transaction = new Transaction();
		try {
			urlcx = new URL(urlcxz);

			URLConnection urlConnection = urlcx.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder = factory.newDocumentBuilder();
			Document document = builder.parse(in);
			document.getDocumentElement().normalize();
			Element root = document.getDocumentElement();

			if(root.getNodeName()==null){
				transaction.setTransactionNull(true);
			}else{
				transaction.setTransactionNull(false);
			}
			NodeList nList = document.getElementsByTagName("transaction");
			Node node = nList.item(0);


			if (node.getNodeType() == Node.ELEMENT_NODE){
				
				Element eElement = (Element) node;
				String status=eElement .getElementsByTagName("status").item(0).getTextContent();
				String referencia=eElement.getElementsByTagName("reference").item(0).getTextContent();
				
				if(status.equalsIgnoreCase("3") || status.equalsIgnoreCase("4")){
					transaction.setStatus(true);
					String[] partes = referencia.split("-");
					transaction.setPlano(partes[1]);
					transaction.setDuracao(Integer.parseInt(partes[2]));
					transaction.setReference(referencia);
					transaction.setServidor(partes[3]);
					
				}else if(status.equalsIgnoreCase("5")){
					transaction.setStatus(false);
					transaction.setError(VipManagerPlus.tagManager.erro()+"Codigo em disputa no PagSeguro, verifique sua conta!");
				}else{
					transaction.setStatus(false);
					transaction.setError(VipManagerPlus.tagManager.erro()+"Codigo n�o autorizado pelo PagSeguro, verifique sua conta!");
				}
			}else{
				transaction.setTransactionNull(true);
				transaction.setError(VipManagerPlus.tagManager.erro()+"Codigo inexistente ou n�o autorizado pelo PagSeguro, verifique sua conta!");
				return transaction;
			}

		} catch (IOException | ParserConfigurationException | SAXException e) {
			transaction.setTransactionNull(true);
			transaction.setError(VipManagerPlus.tagManager.erro()+"Codigo inexistente ou n�o autorizado pelo PagSeguro, verifique sua conta!");
		} catch (NullPointerException e) {
			transaction.setError(VipManagerPlus.tagManager.erro()+"Codigo inexistente ou n�o autorizado pelo PagSeguro, verifique sua conta!");
		}

		return transaction;
	}





}
