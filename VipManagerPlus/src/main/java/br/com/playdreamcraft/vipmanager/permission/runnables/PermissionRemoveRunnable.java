package br.com.playdreamcraft.vipmanager.permission.runnables;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanagerplus.apimanagers.VaultApi;

public class PermissionRemoveRunnable implements Runnable{
	Player player;
	Plugin plugin;
	String plano;
	public PermissionRemoveRunnable(Player player, Plugin plugin, String plano) {
		this.player = player;
		this.plugin = plugin;
		this.plano = plano;
	}
	@Override
	public void run() {
		VaultApi.perms.playerAddGroup(player, plano);
	}
	
}
