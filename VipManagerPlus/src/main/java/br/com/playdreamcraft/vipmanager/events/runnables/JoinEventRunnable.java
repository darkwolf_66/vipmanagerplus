package br.com.playdreamcraft.vipmanager.events.runnables;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.managers.BugManager;
import br.com.playdreamcraft.vipmanager.managers.DatabaseVipManager;
import br.com.playdreamcraft.vipmanager.permission.runnables.PermissionCheckRemoveRunnable;
import br.com.playdreamcraft.vipmanager.permission.runnables.PermissionRemoveRunnable;
import br.com.playdreamcraft.vipmanager.profiles.ReferenceProfile;
import br.com.playdreamcraft.vipmanager.time.DateCalculator;
import br.com.playdreamcraft.vipmanager.tools.ReferenceSplitter;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class JoinEventRunnable implements Runnable{
	Player p;
	Plugin plugin;
	public JoinEventRunnable(Player p, Plugin plugin) {
		this.p = p;
		this.plugin = plugin;
	}
	DateCalculator dateCalc;
	ReferenceSplitter refSplitter;
	ReferenceProfile refProfile;
	BugManager bugManager;
	@Override
	public void run() {
		DatabaseVipManager dbVipManager = new DatabaseVipManager();
		if (dbVipManager.checkNick(p.getName())) {
			if (dbVipManager.checkVipExpiration(p)) {
				dbVipManager.getReference(p.getName());
				refSplitter = new ReferenceSplitter();
				ReferenceProfile refProfile = new ReferenceProfile();
				refProfile = refSplitter.splitReference(dbVipManager.getReference(p.getName()));

				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new PermissionRemoveRunnable(p, plugin, refProfile.getPlano()));

				dbVipManager.setExpired(p);
				p.sendMessage(VipManagerPlus.tagManager.normal()
						+ "Seu VIP expirou!! Voc� pode comprar novamente acessando loja.playdreamcraft.com.br");
				VipManagerPlus.logger.logNormal("Vip de " + p.getName() + "expirou!");
			}
		}else{
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new PermissionCheckRemoveRunnable(p, plugin));
		}

	}
}
