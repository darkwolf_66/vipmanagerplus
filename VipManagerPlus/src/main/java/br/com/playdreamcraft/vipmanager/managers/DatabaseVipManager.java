package br.com.playdreamcraft.vipmanager.managers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.apimanagers.VaultApi;
import br.com.playdreamcraft.vipmanager.mysql.DatabaseProfile;
import br.com.playdreamcraft.vipmanager.mysql.MySQL;
import br.com.playdreamcraft.vipmanager.profiles.ReferenceProfile;
import br.com.playdreamcraft.vipmanager.profiles.VipProfile;
import br.com.playdreamcraft.vipmanager.time.DateCalculator;
import br.com.playdreamcraft.vipmanager.tools.ReferenceSplitter;

public class DatabaseVipManager {
	Player player;
	DatabaseProfile dbProfile;
	MySQL my;
	ReferenceSplitter refSplit;
	public DatabaseVipManager() {
		dbProfile = new DatabaseProfile();
		defineDatabaseProfile();
		my = new MySQL(dbProfile);
	}
	public void defineDatabaseProfile(){
		dbProfile.setDatabase(VipManagerPlus.configManager.getDatabase());
		dbProfile.setIp(VipManagerPlus.configManager.getIp());
		dbProfile.setPort(VipManagerPlus.configManager.getPort());
		dbProfile.setUser(VipManagerPlus.configManager.getUser());
		dbProfile.setPassword(VipManagerPlus.configManager.getPass());
	}
	public boolean addVip(VipProfile vipProfile, Player player){

		VipProfile vipProfileOnline = vipProfileCheck(vipProfile.getUser(), player, true);

		this.player = player;
		if(!vipProfileOnline.isNull()){
			if(vipProfileOnline.getPlano().equalsIgnoreCase(vipProfile.getPlano())){
				return addVipIfExists(vipProfileOnline, vipProfile);
			}
			player.sendMessage(VipManagerPlus.tagManager.erro()+"O seu plano anterior n�o � igual o novo, aguarde o fim do seu vip para ativar!");
			return false;
		}
		StringBuilder vipFinalDate = new StringBuilder();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH-mm");
		Date expirDate;
		try {
			expirDate = new Date(simpleDateFormat.parse(vipProfile.getDataFinal()).getTime());
		} catch (ParseException e1) {
			return false;
		}

		vipFinalDate.append(simpleDateFormat.format(expirDate));
		StringBuilder startDate = new StringBuilder();
		startDate.append(vipProfile.getDataInicial());
		
		String inQuery="INSERT INTO `viplist`(`reference`, `nick`,  `expiration`,`startdate`) VALUES ('"+vipProfile.getReference()+"', '"+vipProfile.getUser()+"',  '"+vipFinalDate+"','"+startDate+"')";
		try {
			my.openConnection();
			if(my.checkConnection()){
				my.updateSQL(inQuery);
				my.closeConnection();
				return true;
			}else{
				return false;
			}


		} catch (ClassNotFoundException e) {
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
	public boolean addVipApi(VipProfile vipProfile, Player player){

		VipProfile vipProfileOnline = vipProfileCheck(vipProfile.getUser(), player, true);

		this.player = player;
		if(!vipProfileOnline.isNull()){
			if(vipProfileOnline.getPlano().equalsIgnoreCase(vipProfile.getPlano())){
				return addVipIfExists(vipProfileOnline, vipProfile);
			}
			player.sendMessage(VipManagerPlus.tagManager.erro()+"O seu plano anterior n�o � igual o novo, aguarde o fim do seu vip para ativar!");
			return false;
		}
		StringBuilder vipFinalDate = new StringBuilder();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH-mm");


		Date expirDate;
		try {
			expirDate = new Date(simpleDateFormat.parse(vipProfile.getDataFinal()).getTime());
		} catch (ParseException e1) {
			return false;
		}

		vipFinalDate.append(simpleDateFormat.format(expirDate));
		StringBuilder startDate = new StringBuilder();
		startDate.append(vipProfile.getDataInicial());
		String inQuery="INSERT INTO `viplist`(`reference`, `nick`,  `expiration`,`startdate`) VALUES ('"+vipProfile.getReference()+"', '"+vipProfile.getUser()+"',  '"+vipFinalDate+"','"+startDate+"')";
		try {
			my.openConnection();
			if(my.checkConnection()){
				my.updateSQL(inQuery);
				my.closeConnection();
				return true;
			}else{
				return false;
			}


		} catch (ClassNotFoundException e) {
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
	public boolean checkRef(String reference){
		String query = "SELECT reference FROM `viplist` WHERE reference='"+reference+"';";

		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			rs.getString("reference");
			my.closeConnection();
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}

	}
	public boolean checkNick(String nick){
		String query = "SELECT nick FROM `viplist` WHERE nick='"+nick+"';";

		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			rs.getString("nick");
			my.closeConnection();
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}

	}
	public String getReference(String nick){
		String query = "SELECT reference FROM `viplist` WHERE nick='"+nick+"';";

		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			String ref = rs.getString("reference");
			my.closeConnection();
			return ref;
		} catch (ClassNotFoundException | SQLException e) {
			return null;
		}
	}
	public String getPlano(String nick){
		String query = "SELECT reference FROM `viplist` WHERE nick='"+nick+"';";
		refSplit = new ReferenceSplitter();
		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			String ref = rs.getString("reference");
			my.closeConnection();
			return refSplit.splitReference(ref).getPlano();
			
		} catch (ClassNotFoundException | SQLException e) {
			return null;
		}
	}

	public boolean addVipIfExists(VipProfile vipProfileOnline, VipProfile vipProfile){
		DateCalculator dateCalc = new DateCalculator();
		Calendar c = Calendar.getInstance();
		c.setTime(dateCalc.stringToDate(vipProfileOnline.getDataFinal()));
		c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
		vipProfile.setDataFinal(new SimpleDateFormat("dd/MM/yyyy-HH-mm").format(c.getTime()));

		StringBuilder vipFinalDate = new StringBuilder();
		vipFinalDate.append(vipProfile.getDataFinal());
		StringBuilder startDate = new StringBuilder();
		startDate.append(vipProfile.getDataInicial());

		String upQuery="UPDATE `viplist` SET `nick`='("+vipProfileOnline.getUser()+")-desativado' WHERE `reference`='"+vipProfileOnline.getReference()+"'";
		String inQuery="INSERT INTO `viplist`(`reference`, `nick`,  `expiration`,`startdate`) VALUES ('"+vipProfile.getReference()+"', '"+vipProfile.getUser()+"',  '"+vipFinalDate+"','"+startDate+"')";
		try {
			my.openConnection();
			if(my.checkConnection()){
				my.updateSQL(inQuery);
				my.updateSQL(upQuery);
				my.closeConnection();
				return true;
			}else{
				return false;

			}


		} catch (ClassNotFoundException e) {
			return false;
		} catch (SQLException e) {
			return false;
		}
	}
	DatabaseVipManager vipManager;
	
	public VipProfile vipProfileCheck(String nick, Player player, boolean api){
		String query = "SELECT * FROM `viplist` WHERE nick='"+nick+"';";

		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			if(rs.getString("expiration").isEmpty()){
				return null;
			}
			ReferenceSplitter refSplitter = new ReferenceSplitter();
			ReferenceProfile refProfile = new ReferenceProfile();
			refProfile = refSplitter.splitReference(rs.getString("reference"));

			DateCalculator dateCalc = new DateCalculator();
			int restante = dateCalc.calcularTempoParaExpirar(dateCalc.stringToDate(rs.getString("expiration")));
			
			if(dateCalc.stringToDate(rs.getString("expiration")).before(dateCalc.dataAtual())){
				vipManager = new DatabaseVipManager();
				vipManager.setExpired(player);
				VaultApi.perms.playerAddGroup(player, refProfile.getPlano());
				player.sendMessage(VipManagerPlus.tagManager.normal()+"Vip Expirado!!! Para comprar mais shop.playdreamcraft.com.br");
			}
			
			
			if(restante < 0){
				player.sendMessage(VipManagerPlus.tagManager.normal()+"Erro no servidor! Contate o administrador!");
				return null;
			}

			VipProfile vipProfile = new VipProfile(nick,refProfile.getServidor() , refProfile.getPlano(), refProfile.getDuracao(), refProfile.getReference(), rs.getString("expiration"), rs.getString("startdate"));
			vipProfile.setTempoRestante(restante);

			return vipProfile;
		} catch (ClassNotFoundException e1) {


		} catch (SQLException e1) {

		}
		VipProfile vipProfile = new VipProfile("null","null", "null", 0, "null", "null", "null");
		vipProfile.setNull(true);
		return vipProfile;
	}
	public boolean checkVipExpiration(Player p){
		String query = "SELECT * FROM `viplist` WHERE nick='"+p.getName()+"';";

		try {
			my.openConnection();
			ResultSet rs= my.querySQL(query);
			rs.next();
			DateCalculator dateCalc = new DateCalculator();
			String result = rs.getString("expiration");
			my.closeConnection();
			return dateCalc.isExpired(result);

		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}
	}
	public boolean setExpired(Player player){
		String upQuery="UPDATE `viplist` SET `nick`='("+player.getName()+")-desativado' WHERE `nick`='"+player.getName()+"'";
		try {
			my.openConnection();
			if(my.checkConnection()){
				my.updateSQL(upQuery);
				my.closeConnection();
				return true;
			}else{
				return false;
			}
		} catch (ClassNotFoundException e) {
			return false;
		} catch (SQLException e) {
			return false;
		}
	}
}
