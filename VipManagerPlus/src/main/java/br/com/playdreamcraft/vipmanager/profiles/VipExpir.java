package br.com.playdreamcraft.vipmanager.profiles;

public class VipExpir {
	private String reference;
	private int restante;
	public VipExpir(String reference, int restante) {
		this.reference = reference;
		this.restante = restante;
	}
	
	
	
	public int getRestante() {
		return restante;
	}
	public void setRestante(int restante) {
		this.restante = restante;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
}
