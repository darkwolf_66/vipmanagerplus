package br.com.playdreamcraft.vipmanager.tools;

import br.com.playdreamcraft.vipmanager.profiles.ReferenceProfile;

public class ReferenceSplitter {
	public ReferenceSplitter() {
		// TODO Auto-generated constructor stub
	}
	public ReferenceProfile splitReference(String reference){
		ReferenceProfile referenceProfile = new ReferenceProfile();
		String[] partes = reference.split("-");
		String plano = partes[1];
		String duracao = partes[2];
		String servidor = partes[3];
		referenceProfile.setDuracao(Integer.parseInt(duracao));
		referenceProfile.setPlano(plano);
		referenceProfile.setServidor(servidor);
		referenceProfile.setReference(reference);
		return referenceProfile;
	}
}
