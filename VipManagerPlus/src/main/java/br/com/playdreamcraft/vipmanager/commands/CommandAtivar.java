package br.com.playdreamcraft.vipmanager.commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.commands.runnables.AtivarThread;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.system.CodeCheck;

public class CommandAtivar implements CommandExecutor {
	Plugin p;
	public CommandAtivar(Plugin p){
		this.p = p;
	}
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		if(!(cs instanceof Player)) {
			Bukkit.getLogger().info("Erro somente players podem executar esse comando!!!");
			return true;
		}
		Player player = (Player)cs;
		if(isNewVersion(args)){
			CodeCheck.getInstance().codeCheck(args, player);
			return true;
		}
		
		if(args.length <= 0){
			player.sendMessage(VipManagerPlus.tagManager.erro()+"Codigo em branco: Digite /ativar codigo");
			return true;
		}
		
		player.sendMessage(VipManagerPlus.tagManager.normal()+"Verificando seu codigo");
		p.getServer().getScheduler().runTaskAsynchronously(p, new AtivarThread(player, args, p));
		
		return true;
	}
	StringBuilder sb;
	public boolean isNewVersion(String[] args){
		for(String arg: args){
			sb.append(sb);
		}
		return sb.toString().matches("[0-9]");
	}
}

