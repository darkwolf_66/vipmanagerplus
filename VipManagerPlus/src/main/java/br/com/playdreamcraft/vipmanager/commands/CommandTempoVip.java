package br.com.playdreamcraft.vipmanager.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.commands.runnables.TempoVipThread;
import br.com.playdreamcraft.vipmanager.commands.runnables.managers.TempoVipManager;

public class CommandTempoVip implements CommandExecutor {
	Plugin p;
	public CommandTempoVip(Plugin p){
		this.p = p;
	}
	TempoVipManager tempoVipManager;
	Player player;
	public boolean onCommand(CommandSender cs, Command cmd, String alias, String[] args) {
		tempoVipManager = new TempoVipManager();
		if(!tempoVipManager.isPlayer(cs))
			return true;
		player = (Player)cs;
		p.getServer().getScheduler().runTaskAsynchronously(p, new TempoVipThread(player));
		return true;
	}
}
