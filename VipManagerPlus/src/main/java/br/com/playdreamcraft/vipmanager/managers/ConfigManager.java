package br.com.playdreamcraft.vipmanager.managers;

import java.io.File;

import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.mysql.DatabaseProfile;

public class ConfigManager {
	private String servidorcg;
	private String ip;
	private String port;
	private String database;
	private String user;
	private String pass;
	private DatabaseProfile dbShopProfile;
	Plugin p;
	public ConfigManager(Plugin p) {
		this.p = p;
	}
	
	public void loadConfigs(){
		verifyConfig();
		servidorcg = p.getConfig().getString("servidor");
		ip = p.getConfig().getString("mysql.host");
		port =  p.getConfig().getString("mysql.port");
		database = p.getConfig().getString("mysql.database");
		user = p.getConfig().getString("mysql.user");
		pass = p.getConfig().getString("mysql.pass");
		dbShopProfile = new DatabaseProfile();
		dbShopProfile.setIp(p.getConfig().getString("mysqlshop.host"));
		dbShopProfile.setPort(p.getConfig().getString("mysqlshop.port"));
		dbShopProfile.setDatabase(p.getConfig().getString("mysqlshop.database"));
		dbShopProfile.setUser(p.getConfig().getString("mysqlshop.user"));
		dbShopProfile.setPassword(p.getConfig().getString("mysqlshop.pass"));
	}
	public void verifyConfig(){
		
		if(!p.getDataFolder().exists()){
			p.getDataFolder().mkdir();
		}
		File configFile = new File(p.getDataFolder(), "config.yml");
		if(!configFile.exists()){
			p.getConfig().addDefault("mysql.host", "example_host");
			p.getConfig().addDefault("mysql.port", "example_port");
			p.getConfig().addDefault("mysql.database", "example_database");
			p.getConfig().addDefault("mysql.user", "example_user");
			p.getConfig().addDefault("mysql.pass", "example_pass");
			p.getConfig().addDefault("servidor", "example_servidor");
			
			p.getConfig().addDefault("mysqlshop.host", "example_host");
			p.getConfig().addDefault("mysqlshop.port", "example_port");
			p.getConfig().addDefault("mysqlshop.database", "example_database");
			p.getConfig().addDefault("mysqlshop.user", "example_user");
			p.getConfig().addDefault("mysqlshop.pass", "example_pass");
			p.getConfig().options().copyDefaults(true);
		    p.saveConfig();
		}
	}

	public String getServidorcg() {
		return servidorcg;
	}

	public void setServidorcg(String servidorcg) {
		this.servidorcg = servidorcg;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public final DatabaseProfile getDbShopProfile() {
		return dbShopProfile;
	}

	public final void setDbShopProfile(DatabaseProfile dbShopProfile) {
		this.dbShopProfile = dbShopProfile;
	}
	
	
	
	
}
