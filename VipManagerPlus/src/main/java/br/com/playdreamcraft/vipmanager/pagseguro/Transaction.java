package br.com.playdreamcraft.vipmanager.pagseguro;

public class Transaction {
	public Transaction() {
	}
	private String reference;
	private boolean transactionNull;
	private boolean status;
	private String error;
	private String plano;
	private int duracao;
	private String servidor;
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getPlano() {
		return plano;
	}
	public void setPlano(String plano) {
		this.plano = plano;
	}
	public int getDuracao() {
		return duracao*30;
	}
	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}
	public String getServidor() {
		return servidor;
	}
	public void setServidor(String servidor) {
		this.servidor = servidor;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public boolean isTransactionNull() {
		return transactionNull;
	}
	public void setTransactionNull(boolean transactionNull) {
		this.transactionNull = transactionNull;
	}
	 
	
	
	
}
