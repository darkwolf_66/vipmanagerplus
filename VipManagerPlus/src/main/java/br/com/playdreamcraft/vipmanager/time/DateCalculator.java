package br.com.playdreamcraft.vipmanager.time;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateCalculator {
	String dataFinal;
	TimeUpdater timeUpdater;
	Calendar cGetDateFinal;
	SimpleDateFormat dateFormater;
	public DateCalculator() {
		
	}
	
	public String getFinalDate(int duracao){
		timeUpdater = new TimeUpdater();
		cGetDateFinal = Calendar.getInstance();
		cGetDateFinal.setTime(timeUpdater.getTime());
		cGetDateFinal.add(Calendar.DATE, duracao);
		dateFormater = new SimpleDateFormat("dd/MM/yyyy-HH-mm");
		dataFinal = dateFormater.format(cGetDateFinal.getTime());

		return dataFinal;
	}
	public Date stringToDate(String expirationDate){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy-HH-mm");  
		try {
			Date date = (Date)formatter.parse(expirationDate);
			return date;
		} catch (ParseException e) {
			return null;
		}
	}
	public boolean verifyDate(Date expirationDate){
		TimeUpdater timeUpdater = new TimeUpdater();
		return timeUpdater.getTime().after(expirationDate);
	}
	public int calcularTempoParaExpirar(Date expirationDate){
		TimeUpdater timeUpdater = new TimeUpdater();
		return dataDiff(timeUpdater.getTime(),expirationDate);
	}

	public static int dataDiff(Date dataLow, Date dataHigh){  

		GregorianCalendar startTime = new GregorianCalendar();  
		GregorianCalendar endTime = new GregorianCalendar();  

		GregorianCalendar curTime = new GregorianCalendar();  
		GregorianCalendar baseTime = new GregorianCalendar();  

		startTime.setTime(dataLow);  
		endTime.setTime(dataHigh);  

		int dif_multiplier = 1;  

		if(dataLow.compareTo(dataHigh) < 0 ){  
			baseTime.setTime(dataHigh);  
			curTime.setTime(dataLow);  
			dif_multiplier = 1;  
		}else{  
			baseTime.setTime(dataLow);  
			curTime.setTime(dataHigh);  
			dif_multiplier = -1;  
		}  

		int result_years = 0;  
		int result_months = 0;  
		int result_days = 0;  

		while( curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR) ||  
				curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH)  )  
		{  

			int max_day = curTime.getActualMaximum( GregorianCalendar.DAY_OF_MONTH );  
			result_months += max_day;  
			curTime.add(GregorianCalendar.MONTH, 1);  

		}  

		result_months = result_months*dif_multiplier;  


		result_days += (endTime.get(GregorianCalendar.DAY_OF_MONTH) - startTime.get(GregorianCalendar.DAY_OF_MONTH));  

		int fim= result_years+result_months+result_days;
		if(fim < 0){
			fim = 0;
		}

		return fim;
	} 

	public boolean isExpired(String expiration){

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH-mm");
		try {
			Date expirDate = new Date(simpleDateFormat.parse(expiration).getTime());
			if(expirDate.before(dataAtual())){
				return true;
			}else{
				return false;
			}
		} catch (ParseException e) {
			return false;
		}
	}
	public Date dataAtual(){
		timeUpdater = new TimeUpdater();
		return timeUpdater.getTime();
	}

}
