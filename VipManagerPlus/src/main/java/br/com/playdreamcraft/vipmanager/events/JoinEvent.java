package br.com.playdreamcraft.vipmanager.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.events.runnables.JoinEventRunnable;

public class JoinEvent implements Listener {
	Plugin plugin;

	public JoinEvent(Plugin p) {
		plugin = p;
	}
	Player player;
	@EventHandler
	public void onJoinGame(PlayerJoinEvent e) {
		
		player = e.getPlayer();
		plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new JoinEventRunnable(player, plugin));
		
	}
}
