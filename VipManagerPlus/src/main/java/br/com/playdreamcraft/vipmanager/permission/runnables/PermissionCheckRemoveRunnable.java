package br.com.playdreamcraft.vipmanager.permission.runnables;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanagerplus.apimanagers.VaultApi;

public class PermissionCheckRemoveRunnable implements Runnable{
	Player player;
	Plugin plugin;
	String plano;
	public PermissionCheckRemoveRunnable(Player player, Plugin plugin) {
		this.player = player;
		this.plugin = plugin;
	}
	@Override
	public void run() {
		
		if(player.hasPermission("obsidian")){
			plano = "obsidian";
			VaultApi.perms.playerRemoveGroup(player, plano);
		}else if(player.hasPermission("esmeralda")){
			plano = "esmeralda";
			VaultApi.perms.playerRemoveGroup(player, plano);
		}else if(player.hasPermission("diamante")){
			plano = "diamante";
			VaultApi.perms.playerRemoveGroup(player, plano);
		}else if(player.hasPermission("ouro")){
			plano = "ouro";
			VaultApi.perms.playerRemoveGroup(player, plano);
		}else if(player.hasPermission("ferro")){
			plano = "ferro";
			VaultApi.perms.playerRemoveGroup(player, plano);
		}
	}

}
