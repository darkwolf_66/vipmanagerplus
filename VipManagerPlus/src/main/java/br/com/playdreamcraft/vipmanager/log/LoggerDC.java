package br.com.playdreamcraft.vipmanager.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.time.DateCalculator;

public class LoggerDC {	
	Plugin plugin = null;
	public LoggerDC(Plugin plugin){
		this.plugin = plugin;
	}
	public void logNormal(String message){
		DateCalculator dateCalc = new DateCalculator();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String date = dateFormater.format(dateCalc.dataAtual()).toString();
		try{
			File dataFolder = plugin.getDataFolder();
			File logFolder = new File(plugin.getDataFolder(), "logs");
			if(!dataFolder.exists()){
				dataFolder.mkdir();
				logFolder.mkdir();
			}else if(!logFolder.exists()){
				logFolder.mkdir();
			}
			File saveTo = new File(logFolder, "vip.log");
			if (!saveTo.exists()){
				saveTo.createNewFile();
			}
			FileWriter fw = new FileWriter(saveTo, true);
			PrintWriter pw = new PrintWriter(fw);
			pw.println(date+" - "+message);
			pw.flush();
			pw.close();
		} catch (IOException e){
			e.printStackTrace();
		}

	}
	public void logError(String message){
		DateCalculator dateCalc = new DateCalculator();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String date = dateFormater.format(dateCalc.dataAtual()).toString();
		try{
			File dataFolder = plugin.getDataFolder();
			File logFolder = new File(plugin.getDataFolder(), "logs");
			if(!dataFolder.exists()){
				dataFolder.mkdir();
				logFolder.mkdir();
			}else if(!logFolder.exists()){
				logFolder.mkdir();
			}
			File saveTo = new File(logFolder, "errors.log");
			if (!saveTo.exists()){
				saveTo.createNewFile();
			}
			FileWriter fw = new FileWriter(saveTo, true);
			PrintWriter pw = new PrintWriter(fw);
			pw.println(date+" - "+message);
			pw.flush();
			pw.close();
		} catch (IOException e){
			e.printStackTrace();
		}

	}
}
