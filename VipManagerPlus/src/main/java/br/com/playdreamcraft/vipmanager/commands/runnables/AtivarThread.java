package br.com.playdreamcraft.vipmanager.commands.runnables;

import java.text.SimpleDateFormat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import br.com.playdreamcraft.vipmanager.managers.DatabaseVipManager;
import br.com.playdreamcraft.vipmanager.pagseguro.Transaction;
import br.com.playdreamcraft.vipmanager.pagseguro.TransactionManager;
import br.com.playdreamcraft.vipmanager.permission.runnables.PermissionsAddRunnable;
import br.com.playdreamcraft.vipmanager.profiles.VipProfile;
import br.com.playdreamcraft.vipmanager.time.DateCalculator;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class AtivarThread implements Runnable {
	Player player;
	String[] args;
	Plugin plugin;
	public AtivarThread(Player player, String[] args, Plugin plugin) {
		this.player = player;
		this.args = args;
		this.plugin = plugin;
	}

	@Override
	public void run() {
		TransactionManager transactionManager = new TransactionManager();
		StringBuilder sBuilder = new StringBuilder();
		for(int i = 0; i < args.length; i++){
			if(i == 0){
				sBuilder.append(args[i]);
			}else{
				sBuilder.append("%20" + args[i]);
			}
		}
		Transaction transaction = transactionManager.consultar(sBuilder.toString(), player);

		if (transaction.isTransactionNull()) {
			player.sendMessage(transaction.getError());
		} else if (!transaction.isStatus()) {
			player.sendMessage(transaction.getError());
		} else {
			String plano = transaction.getPlano();
			int duracao = transaction.getDuracao();
			String servidor = transaction.getServidor();
			String reference = transaction.getReference();
			DateCalculator dateCalculator = new DateCalculator();
			if (!servidor.equalsIgnoreCase(VipManagerPlus.configManager.getServidorcg())
					&& !servidor.equalsIgnoreCase("todos")) {
				player.sendMessage(VipManagerPlus.tagManager.erro() + "Erro: O vip � para o servidor " + servidor);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy-HH-mm");
				VipProfile vipProfile = new VipProfile(player.getName(), servidor, plano, duracao, reference,
						dateCalculator.getFinalDate(duracao) + "", sdf.format(dateCalculator.dataAtual()) + "");

				DatabaseVipManager dbVipManager = new DatabaseVipManager();

				if (dbVipManager.checkRef(reference)) {
					player.sendMessage(VipManagerPlus.tagManager.erro() + "Erro: Codigo em uso!");
				} else if (!dbVipManager.addVip(vipProfile, player)) {

				} else {
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new PermissionsAddRunnable(player, vipProfile.getPlano()));

					Bukkit.getServer().broadcastMessage(VipManagerPlus.tagManager.vipAtivado(plano, player.getName()));
					VipManagerPlus.logger.logNormal("Vip: " + plano + " adicionado para: " + player.getName());
				}
			}
		}
	}
}
