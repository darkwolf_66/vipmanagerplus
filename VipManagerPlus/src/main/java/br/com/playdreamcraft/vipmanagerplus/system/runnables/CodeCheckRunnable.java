package br.com.playdreamcraft.vipmanagerplus.system.runnables;

import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.chat.ChatAlert;
import br.com.playdreamcraft.vipmanagerplus.database.checagem.CodigoMySQL;
import br.com.playdreamcraft.vipmanagerplus.managers.CodeBuilder;
import br.com.playdreamcraft.vipmanagerplus.profiles.CodigoProfile;
import br.com.playdreamcraft.vipmanagerplus.system.runnables.CodeProfilePopulator;

public class CodeCheckRunnable implements Runnable{
	String[] args;
	Player player;
	public CodeCheckRunnable(String[] args, Player player) {
		this.args = args;
		this.player = player;
	}
	CodigoProfile codigoP;
	CodigoMySQL codigoM;
	@Override
	public void run() {
		codigoP = new CodigoProfile();
		//Checa para ver se a order esta pago
		codigoP.setId(CodeBuilder.getInstance().montarCodigo(args));
		codigoP.setStatus(codigoM.checarStatus(codigoP.getId(), player));
		if(codigoP.getStatus() == 100){
			ChatAlert.getInstance().sendPrivateErrorMessage(player, "Erro codigo invalido!");
		}else if (codigoP.getStatus() == 5){
			//
			//Prosegue com o processo
			//
			VipManagerPlus.getInstance().getServer().getScheduler().runTaskAsynchronously(VipManagerPlus.getInstance(), new CodeProfilePopulator(codigoP, player));
			
		}else{
			ChatAlert.getInstance().sendPrivateErrorMessage(player, "A transa��o n�o foi autorizada ainda! Aguarde o pagseguro autorizar!!");
		}
	}
}
