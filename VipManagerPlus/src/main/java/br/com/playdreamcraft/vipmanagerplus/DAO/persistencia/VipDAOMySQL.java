package br.com.playdreamcraft.vipmanagerplus.DAO.persistencia;

public class VipDAOMySQL {
	private static VipDAOMySQL instance;

	private VipDAOMySQL() {
	}

	public static VipDAOMySQL getInstance() {
		if (null == instance) {
			instance = new VipDAOMySQL();
		}
		return instance;
	}
}