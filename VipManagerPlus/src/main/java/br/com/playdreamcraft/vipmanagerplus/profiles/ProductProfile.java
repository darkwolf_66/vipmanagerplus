package br.com.playdreamcraft.vipmanagerplus.profiles;

import java.util.ArrayList;

public class ProductProfile {
	private int product_id;
	private String type;
	private String[] model;
	private int quantity;
	private boolean status;
	private ArrayList<String> nicks;
	
	public ProductProfile(int product_id, String type, String[] model, int quantity, boolean status, ArrayList<String> nicks) {
		this.product_id = product_id;
		this.type = type;
		this.quantity = quantity;
		this.status = status;
		this.nicks = nicks;
		this.model = model;
	}

	public final int getProduct_id() {
		return product_id;
	}

	public final void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public final String getType() {
		return type;
	}

	public final void setType(String type) {
		this.type = type;
	}

	public final int getQuantity() {
		return quantity;
	}

	public final void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public final boolean isStatus() {
		return status;
	}

	public final void setStatus(boolean status) {
		this.status = status;
	}

	public final ArrayList<String> getNicks() {
		return nicks;
	}

	public final void setNicks(ArrayList<String> nicks) {
		this.nicks = nicks;
	}

	public final String[] getModel() {
		return model;
	}

	public final void setModel(String[] model) {
		this.model = model;
	}
	
	
}
