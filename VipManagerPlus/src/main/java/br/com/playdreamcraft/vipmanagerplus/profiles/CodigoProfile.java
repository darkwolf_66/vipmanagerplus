package br.com.playdreamcraft.vipmanagerplus.profiles;

import java.util.ArrayList;

import br.com.playdreamcraft.vipmanagerplus.interfaces.Codigo;

public class CodigoProfile implements Codigo{
	private String id;
	private int status;
	private ArrayList<ProductProfile> products;
	public final String getId() {
		return id;
	}
	public final void setId(String id) {
		this.id = id;
	}
	public final int getStatus() {
		return status;
	}
	public final void setStatus(int status) {
		this.status = status;
	}
	public final ArrayList<ProductProfile> getProducts() {
		return products;
	}
	public final void setProducts(ArrayList<ProductProfile> products) {
		this.products = products;
	}
	@Override
	public String getCodigo() {
		return id;
	}
	@Override
	public void setCodigo(String codigo) {
		id = codigo;
	}
	
}
