package br.com.playdreamcraft.vipmanagerplus.interfaces;

public interface Codigo {
	public String getCodigo();
	public void setCodigo(String codigo);
}
