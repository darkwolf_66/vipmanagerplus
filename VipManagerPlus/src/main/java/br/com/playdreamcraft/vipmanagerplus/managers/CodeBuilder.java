package br.com.playdreamcraft.vipmanagerplus.managers;

public class CodeBuilder {
	private static CodeBuilder instance;

	private CodeBuilder() {
	}

	public static CodeBuilder getInstance() {
		if (null == instance) {
			instance = new CodeBuilder();
		}
		return instance;
	}
	private StringBuilder codigo;
	public String montarCodigo(String[] args){
		for(String arg: args){
			codigo.append(arg);
		}
		return codigo.toString();
	}
}
