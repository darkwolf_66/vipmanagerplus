package br.com.playdreamcraft.vipmanagerplus.managers;

public class InstanceManager {
	private static InstanceManager instance;

	private InstanceManager() {
		
	}

	public static InstanceManager getInstance() {
		if (null == instance) {
			instance = new InstanceManager();
		}
		return instance;
	}
	
}
