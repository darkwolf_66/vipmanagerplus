package br.com.playdreamcraft.vipmanagerplus;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import br.com.playdreamcraft.vipmanager.commands.CommandAddVip;
import br.com.playdreamcraft.vipmanager.commands.CommandAtivar;
import br.com.playdreamcraft.vipmanager.commands.CommandTempoVip;
import br.com.playdreamcraft.vipmanager.log.LoggerDC;
import br.com.playdreamcraft.vipmanager.managers.ConfigManager;
import br.com.playdreamcraft.vipmanager.managers.ListenerManager;
import br.com.playdreamcraft.vipmanager.managers.TagManager;

public class VipManagerPlus extends JavaPlugin {
	public static LoggerDC logger;
	ListenerManager listenerManager;
	public static ConfigManager configManager;
	public static TagManager tagManager;
	public void onEnable(){
		//ListenerManager
		listenerManager = new ListenerManager(this);
		listenerManager.registerEvents();
		//TagManager
		tagManager = new TagManager();
		//ConfigManager
		configManager = new ConfigManager(this);
		configManager.loadConfigs();
		loadCommands();
		//LogManager
		logger = new LoggerDC(this);
	}
	public void onDisable(){
		HandlerList.unregisterAll(this);
	}

	public void loadCommands(){
		getCommand("ativar").setExecutor(new CommandAtivar(this));
		getCommand("tempovip").setExecutor(new CommandTempoVip(this));
		getCommand("addvip").setExecutor(new CommandAddVip(this));
	}
	public static Plugin getInstance(){
		return Bukkit.getPluginManager().getPlugin("VipManagerPlus");
	}
}
