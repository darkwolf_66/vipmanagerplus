package br.com.playdreamcraft.vipmanagerplus.apimanagers;

import java.util.logging.Logger;

import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class VaultApi {
	private static VaultApi instance;
	private static final Logger log = Logger.getLogger("Minecraft");
    public static Economy econ = null;
    public static Permission perms = null;
    public static Chat chat = null;
	private VaultApi() {
		if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", VipManagerPlus.getInstance().getDescription().getName()));
            VipManagerPlus.getInstance().getServer().getPluginManager().disablePlugin(VipManagerPlus.getInstance());
            return;
        }
        setupPermissions();
        setupChat();
	}

	public static VaultApi getInstance() {
		if (null == instance) {
			instance = new VaultApi();
		}
		return instance;
	}
	private boolean setupEconomy() {
        if (VipManagerPlus.getInstance().getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = VipManagerPlus.getInstance().getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = VipManagerPlus.getInstance().getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = VipManagerPlus.getInstance().getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
}
