package br.com.playdreamcraft.vipmanagerplus.profiles;

public class ModelProfile {
	private String type;
	private String plano;
	private String server;
	public final String getType() {
		return type;
	}
	public final void setType(String type) {
		this.type = type;
	}
	public final String getPlano() {
		return plano;
	}
	public final void setPlano(String plano) {
		this.plano = plano;
	}
	public final String getServer() {
		return server;
	}
	public final void setServer(String server) {
		this.server = server;
	}
	
}
