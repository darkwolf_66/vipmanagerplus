package br.com.playdreamcraft.vipmanagerplus.system;

import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.system.runnables.CodeCheckRunnable;

public class CodeCheck {
	private static CodeCheck instance;

	private CodeCheck() {
		
	}

	public static CodeCheck getInstance() {
		if (null == instance) {
			instance = new CodeCheck();
		}
		return instance;
	}
	public void codeCheck(String[] args, Player player){
		VipManagerPlus.getInstance().getServer().getScheduler().runTaskAsynchronously(VipManagerPlus.getInstance(), new CodeCheckRunnable(args, player));
	}
}
