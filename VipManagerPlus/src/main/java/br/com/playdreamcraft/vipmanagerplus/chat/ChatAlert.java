package br.com.playdreamcraft.vipmanagerplus.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;

public class ChatAlert {
	private static ChatAlert instance;

	private ChatAlert() {
	}

	public static ChatAlert getInstance() {
		if (null == instance) {
			instance = new ChatAlert();
		}
		return instance;
	}
	public void sendPrivateMessage(Player player, String message){
		player.sendMessage(""+ChatColor.RED+"["+ChatColor.AQUA+"VipManager"+ChatColor.RED+"] "+ChatColor.GREEN+message);
	}
	public void sendGlobalMessage(String message){
		VipManagerPlus.getInstance().getServer().broadcastMessage(""+ChatColor.RED+"["+ChatColor.AQUA+"VipManager"+ChatColor.RED+"] "+ChatColor.GREEN+message);
	}
	public void sendPrivateErrorMessage(Player player, String message){
		player.sendMessage(""+ChatColor.RED+"["+ChatColor.AQUA+"VipManager"+ChatColor.RED+"] "+ChatColor.RED+message);
	}
	public void sendGlobalErrorMessage(String message){
		VipManagerPlus.getInstance().getServer().broadcastMessage(""+ChatColor.RED+"["+ChatColor.AQUA+"VipManager"+ChatColor.RED+"] "+ChatColor.RED+message);
	}
}
