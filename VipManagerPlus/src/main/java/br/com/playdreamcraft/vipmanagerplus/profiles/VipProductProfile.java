package br.com.playdreamcraft.vipmanagerplus.profiles;

import java.util.ArrayList;

public class VipProductProfile extends ProductProfile{
	public VipProductProfile(int product_id, String type, String[] model, int quantity, boolean status,
			ArrayList<String> nicks) {
		super(product_id, type, model, quantity, status, nicks);
		this.plano = plano;
		this.tempo = tempo;
		this.servidor = servidor;
	}
	private String plano;
	private String servidor;
	private int tempo;
	
	public final String getPlano() {
		return plano;
	}
	public final void setPlano(String plano) {
		this.plano = plano;
	}
	public final int getTempo() {
		return tempo;
	}
	public final void setTempo(int tempo) {
		this.tempo = tempo;
	}
	public final String getServidor() {
		return servidor;
	}
	public final void setServidor(String servidor) {
		this.servidor = servidor;
	}	
}
