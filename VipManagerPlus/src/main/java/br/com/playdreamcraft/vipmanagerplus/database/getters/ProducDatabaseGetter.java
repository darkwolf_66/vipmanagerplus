package br.com.playdreamcraft.vipmanagerplus.database.getters;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.entity.Player;

import br.com.playdreamcraft.vipmanager.mysql.MySQL;
import br.com.playdreamcraft.vipmanagerplus.VipManagerPlus;
import br.com.playdreamcraft.vipmanagerplus.chat.ChatAlert;
import br.com.playdreamcraft.vipmanagerplus.generators.ModelGenerator;
import br.com.playdreamcraft.vipmanagerplus.profiles.ProductProfile;

public class ProducDatabaseGetter {
	MySQL my;
	ResultSet rs;
	public ProducDatabaseGetter() {
		my = new MySQL(VipManagerPlus.configManager.getDbShopProfile());
	}
	ArrayList<ProductProfile> products;
	ModelGenerator modelgen;
	String[] model;
	ArrayList<String> nicks;
	public ArrayList<ProductProfile> getProductsFromDatabase(String codigoD){
		try {
			rs = my.querySQL("SELECT * FROM dc_order_product WHERE order_id ='"+codigoD+"';");
			while(rs.next()){
				modelgen = new ModelGenerator();
				model = modelgen.generateModel(rs.getString("model"));
				boolean status = false;
				if(rs.getInt("status") == 1){
					status = true;
				}
				
				products.add(
						new ProductProfile(rs.getInt("order_product_id"), model[0], model, rs.getInt("quantity"), status, nicks));
			}
			
			
			return products;
		} catch (ClassNotFoundException e) {
			return null;
		} catch (SQLException e) {
			return null;
		}
	}
}
